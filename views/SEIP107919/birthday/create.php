<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
         <title>Birthday List</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
        
        
    </head>
    <body>
     <div class="container">
       <div class="jumbotron">  
        <header>
            <center>
                <h1>Birthday Entry</h1>
            </center>
        </header>
        <hr>
        </div>
        <center>
            
            <form method="post" action="store.php" role="form" >
               <div class="form-group">
                  <label for="name">Name:</label>
                  <input type="text"name="name" class="form-control" id="name"> 
               </div>
               <div class="form-group">
                  <label for="birthday">Birthday:</label>
                  <input type="date" name="birthday" class="form-control" id="birthday"> 
               </div>
               <div class="checkbox">
                   <label><input type="checkbox"> Remember me</label>
               </div>
                   <button class="btn btn-info"type="reset" class="btn btn-default">reset</button>
                   <button class="btn btn-primary"type="submit" class="btn btn-default">Submit</button> 
                
             </form>
            
        </div>  
    
    <script src="../js/bootstrap-min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
