<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107919\terms\Terms;

$terms = new Terms();
$var =$terms->edit($_GET['id']);

?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
       
        <title>Terms & Condition</title>
      
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
              
    </head>
    <body>
        <div class="container">
        <header>
            <center>
                <h1>Update Terms & Condition</h1>
            </center>
        </header>
        <hr>
        <center>
            <div id="form">
                <ul class="pager">
                  <li class="previous"><a href="index.php">Back</a></li>
                </ul>
             <form method="post" action="update.php" role="form" >
                 <input type="hidden" name="id" class="form-control" id="name" value="<?php echo $var->id;?>"> 
               <div class="form-group">
                  <label for="name"> Name:</label>
                  <input type="text" name="name" class="form-control" id="name" value="<?php echo $var->name;?>"> 
               </div>
                <div class="form-group">
                <label for="name">Terms & Condition:</label>
               <input type="radio" name="terms" value="Accepted" <?php if($var->terms=="Accepted"){echo "checked";}?>  required />Agree
               <input type="radio" name="terms" value="Not Accepted" <?php if($var->terms=="Not Accepted"){echo "checked";}?>  required /> Not Agree
                </div>
                  <button class="btn btn-info"type="reset" class="btn btn-default">reset</button>
                   <button class="btn btn-primary"type="submit" class="btn btn-default">Submit</button> 
                   
             </form>
            
       
            
            </div>
         </center>      
                
               
        </div>
        <script src="../js/bootstrap.js"></script>
        <script src="../jslib/dist/sweetalert.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
<?php

