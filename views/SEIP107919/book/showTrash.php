<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107919\book\book;
use ATOMIC12\BITM\seip107919\Message\message;
use ATOMIC12\BITM\seip107919\Utility\Utility;

$book = new Book();
$var =$book->showTrash();

?>
<html>
    <head>
     
        <title>Book Title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
        
        
    </head>
    <body>
  <div class="container">
        <header>
            <center>
                <h1>Welcome Come to Book List</h1>
            </center>
        </header>
        <hr>
        <ul class="pager">
                  <li class="previous"><a href="http://localhost/atomic12">Back</a></li>
                </ul>
     <center>
             
   <a class="btn btn-primary" href="create.php"><b>Create</b></a>
     <a class="btn btn-primary" href="recover.php"><b>Restore</b></a>
                         <div>
                            <?php echo Message::flash();?>.
                          </div> 
                    
  <table class="table table-bordered">
    <thead>
      <tr class="success">
        <th>Serial</th>
        <th>Name</th>
        <th>Author</th>
        <th>Action</th>
      </tr>
    </thead>
            
           <?php $slno=0;foreach($var as $book): $slno++ ?>
           <tr class="info">
               <td><?php echo $slno;?></td>
               <td><?php echo $book['name'];?></td><td><?php echo $book['author'];?>
               </td><td><a class="btn btn-success" href="view.php?id=<?php echo $book['id'];?>">View</a> 
                <a class="btn btn-info" href="edit.php?id=<?php echo $book['id'];?>">Edit</a> 
                <a class="btn btn-danger" href="delete.php?id=<?php echo $book['id'];?>">Delete</a>
                <a class="btn btn-primary" href="mail.php?id=<?php echo $book['id'];?>">Email To Friend</a>
               </td></tr>
            <?php endforeach;?>
 </table>
            
       </center>
 </div> 
    <script src="../js/bootstrap-min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
