<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107919\city\City;
use ATOMIC12\BITM\seip107919\Message\message;
use ATOMIC12\BITM\seip107919\Utility\Utility;

$city = new City();
$var =$city->index();

?>




<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>City List</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
        
    </head>
    <body>
  <div class="container">
        <header>
            <center>
                <h1>Welcome Come to City List</h1>
            </center>
        </header>
        <hr>
              <ul class="pager">
                  <li class="previous"><a href="http://localhost/atomic12">Back</a></li>
                </ul>
     
     <center>
             
  <a class="btn btn-primary" href="create.php"><b>Create</b></a>
                        <div>
                            <?php echo Message::flash();?>.
                          </div> 
                    
 <table class="table table-bordered">
    <thead>
      <tr class="success">
        <th>Serial</th>
        <th>Name</th>
        <th>City</th>
        <th>Action</th>
      </tr>
    </thead>     
          <?php $slno=0; foreach ($var as $city):$slno++ ?>
          <tr class="info">
         <tr><td><?php echo $slno;?></td><td><?php echo $city['name'];?></td><td><?php echo $city['city'];?></td>
         <td><a class="btn btn-success" href="view.php?id=<?php echo $city['id'];?>">View</a> 
             <a class="btn btn-info" href="edit.php?id=<?php echo $city['id'];?>">Edit</a> 
             <a class="btn btn-danger" href="delete.php?id=<?php echo $city['id'];?>">Delete</a></td></tr>
          <?php endforeach;?>
                
 </table>
            
       </center>
  </div>
    <script src="../js/bootstrap-min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
