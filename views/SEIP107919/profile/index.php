<?php

include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107919\profile\Profile;

$profile = new Profile();
$var=$profile->index();

?>




<!DOCTYPE html>
<html>
    <head>
       
        <title>Profile Information</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
    </head>
    <body>
       
        
       <div class="col-sm-2">
                
            </div>
            <center>
             
                   
                    
                    
               
                        <div class="panel-heading">
                           <h1>Profile Information</h1> 
                            <ul class="pager">
                                
                                <li class="previous"><a href="http://localhost/atomic12">Back</a></li>
                                <li class="previous"></li>   
                                <li class="next"><a href="create.php">Create New</a></li>
                           </ul>
                            
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                               <tr class="success"><th width="50">Serial</th><th>Name</th><th>Image</th><th>Action</th></tr>
                               <?php $slno=0; foreach($var as $profile): $slno++ ?>
                               <tr><td><?php echo $slno;?></td>
                                   <td><?php echo $profile['name'];?></td>
                                   <td width="100"> <?php echo "<img class='img-rounded' alt='Cinque Terre' width='80' height='50' src='userphoto/".$profile['image']."'  />"; ?>
                                   </td>
                                     <td width="190">
                                         <div class="btn-group">
                                            <a class="btn btn-primary" href="view.php?id=<?php echo $profile['id'];?>">View</a>
                                            <a class="btn btn-success" href="edit.php?id=<?php echo $profile['id'];?>">Edit</a>
                                            <a class="btn btn-danger" href="delete.php?id=<?php echo $profile['id'];?>">Delete</a>
                                         </div>
                                     </td>
                               </tr>
                              <?php endforeach; ?>




                            </table>
                    </div>
               
            
          
       
                
                
               
            </center>
       
      <script src="../js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
