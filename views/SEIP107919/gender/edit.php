<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107919\gender\Gender;

$gender = new Gender();
$var =$gender->edit($_GET['id']);

?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Gender Entry</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
        
        
    </head>
    <body>
        <header>
            <center>
                <h1>Update Gender</h1>
            </center>
        </header>
        <hr>
        <center>
            <div id="form">
                 <ul class="pager">
                  <li class="previous"><a href="index.php">Back</a></li>
                </ul>
            <form method="post" action="update.php" >
                <table>
                    <input type="hidden" name="id" value="<?php echo $var->id;?>">
                 <tr><td>Name :</td><td><input type="text" name="name" value="<?php echo $var->name;?>" placeholder="enter your name here"  required/></td></tr><br>
                <tr><td>Gender :</td><td><input type="radio" name="gender" value="Male" <?php if($var->gender=="Male"){echo "checked";}?>  required />Male<input type="radio" name="gender" value="Female" <?php if($var->gender=="Female"){echo "checked";}?>  required />Female</td></tr><br>
                 <tr><td></td><td><input class="btn-success" type="reset"  value="Reset" />
                <input class="btn-info" type="submit" name="Update" value="Update" /></td></tr>
                
                
                </table>
                
            </form>
            
       
            
            </div>
         </center>      
                
               

    </body>
</html>
