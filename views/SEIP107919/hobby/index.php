<?php
session_start();
include_once($_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."atomic12".DIRECTORY_SEPARATOR."vendor/autoload.php");

use ATOMIC12\BITM\seip107919\hobby\Hobby;
use ATOMIC12\BITM\seip107919\Message\message;
use ATOMIC12\BITM\seip107919\Utility\Utility;

$hobby = new Hobby();
$var =$hobby->index();

?>




<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Hobby List</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css" media="all" />
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css" media="all" />
        
    </head>
    <body>
  <div class="container">
        <header>
            <center>
                <h1>Welcome Come to Hobby List</h1>
            </center>
        </header>
        <hr>
             <ul class="pager">
                  <li class="previous"><a href="http://localhost/atomic12">Back</a></li>
                </ul>
        
            <center>
  <a class="btn btn-primary" href="create.php"><b>Create</b></a>
                         <div>
                            <?php echo Message::flash();?>.
                          </div>
                    
 <table class="table table-bordered">
                               <tr class="info">
                                   <th width="50">Serial</th><th>Person Name</th><th>Hobby</th><th>Action</th></tr>
                               <?php $slno=0; foreach($var as $hobby): $slno++ ?>
                               <tr><td><?php echo $slno;?></td><td><?php echo $hobby['name'];?></td>
                                   <td>
                                       <?php 
                                        echo $hobby['hobby'];
                                        ?>
                                  </td>
                                     <td width="200">
                                         
                                            <a class="btn btn-success" href="view.php?id=<?php echo $hobby['id'];?>">View</a>
                                            <a class="btn btn-info" href="edit.php?id=<?php echo $hobby['id'];?>">Edit</a>
                                            <a class="btn btn-danger" href="delete.php?id=<?php echo $hobby['id'];?>">Delete</a>
                                         
                                     </td>
                               </tr>
                   <?php endforeach;?>
                
  </table>
  </center>
  </div>
    <script src="../js/bootstrap-min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </body>
</html>
